FROM mhart/alpine-node:12

RUN mkdir -p /app

WORKDIR /app

COPY /index.js .
COPY /package.json .

RUN yarn

CMD ["node", "index.js"]

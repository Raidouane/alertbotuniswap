const axios = require('axios');
const {WebClient} = require('@slack/web-api');

let saved = [];
let baseCloneSaved = [];

const web = new WebClient('xoxb-1159168933538-1563304977138-L3xLWV8qCiRmGtRp6AjWnM7w');
const groupId = 'G01GKF6ALP5';

setInterval(async function () {
    getResults();
    getResultsBase();
}, 1000);

const run = async () => {
    web.chat.postMessage({
        text: `Hello, I am the bot`,
        channel: groupId
    });
    await getResults();
    while (true) {
        await new Promise(r => setTimeout(r, 10000));
    }
}

const getResults = async () => {
    const results = await axios({
        method: 'POST',
        url: 'https://api.thegraph.com/subgraphs/name/uniswap/uniswap-v2',
        headers: {
            "content-type": "application/json",
        },
        data: "{\"query\":\"{\\n   tokens(first: 1000, skip: 0, where: {name_contains: \\\"reflect\\\"}) {\\n     id\\n    name\\n    txCount\\n    symbol\\n    tradeVolume\\n   }\\n}\",\"variables\":null}"
    });
    const {tokens} = results.data.data;
    if (saved.length) {
        const newTokens = tokens.filter(tk => !saved.find(s => s.id === tk.id));
        newTokens.forEach((newToken) => {
            const {name, id, txCount, tradeVolume, symbol} = newToken;
            const alert = `🚨 New Reflect clone:\n\`Name\`: *${name}*\n\`Symbol\`: *${symbol}*\n\`Uniswap url\`: https://app.uniswap.org/#/swap?inputCurrency=${id}\n\`Contract Address\`: ${id}\n\`Current Number of TX\`: ${txCount}\n\`Current Trade Volume\`: ${tradeVolume}\n\n\n\n`
            web.chat.postMessage({
                text: alert,
                channel: groupId
            });
        });
    }
    saved = tokens;
}

const getResultsBase = async () => {
    const results = await axios({
        method: 'POST',
        url: 'https://api.thegraph.com/subgraphs/name/uniswap/uniswap-v2',
        headers: {
            "content-type": "application/json",
        },
        data: "{\"query\":\"{\\n   tokens(first: 1000, skip: 0, where: {name_contains: \\\"base\\\"}) {\\n     id\\n    name\\n    txCount\\n    symbol\\n    tradeVolume\\n   }\\n}\",\"variables\":null}"
    });
    const {tokens} = results.data.data;
    console.log('tokens', tokens)
    if (baseCloneSaved.length) {
        const newTokens = tokens.filter(tk => !baseCloneSaved.find(s => s.id === tk.id));
        newTokens.forEach((newToken) => {
            const {name, id, txCount, tradeVolume, symbol} = newToken;
            const alert = `🚨 New Base clone:\n\`Name\`: *${name}*\n\`Symbol\`: *${symbol}*\n\`Uniswap url\`: https://app.uniswap.org/#/swap?inputCurrency=${id}\n\`Contract Address\`: ${id}\n\`Current Number of TX\`: ${txCount}\n\`Current Trade Volume\`: ${tradeVolume}\n\n\n\n`
            web.chat.postMessage({
                text: alert,
                channel: groupId
            });
        });
    }
    baseCloneSaved = tokens;
}

run().then()
